<?php
class PreparePreLanguage {

    protected $html;

    public function __construct($html) {
        $this->html = $html;
    }

    public function addFooter($footerHtml, $oUser) {
        if (!strstr($footerHtml, '$$unsubscribe_link$$') || !strstr($this->html, '$$unsubscribe$$')) {
            return false;
        }
        $this->html = str_replace('$$unsubscribe$$', $footerHtml, $this->html);
        $this->addUnsubscribeLink($oUser);
        return true;
    }

    private function addUnsubscribeLink($oUser) {
        $link = Yii::app()->params['retargetingDomain'] . '/retargetingEngine/unsubscribe?email=' . $oUser->email;
        $this->html = str_replace('$$unsubscribe_link$$', $link, $this->html);
    }

    public function addName($oUser) {
        if ($oUser->name_id) {
            $this->html = str_replace('$$first_name$$', mb_convert_case($oUser->name->name, MB_CASE_TITLE,"UTF-8"), $this->html);
        } else {
            $this->html = str_replace('$$first_name$$', '', $this->html);
        }
    }

    public function getHtml() {
        return $this->html;
    }

}

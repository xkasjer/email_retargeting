<?php
class ButtonGroup {
	
	public static function display(array $buttons) {
		$html = '<div class="btn-row">';
		foreach ($buttons as $button) {
			if(isset($button['visible']) && (!$button['visible'])) {
				continue;
			}
			$text = $button['text'];
			$url = $button['url'];
			if(isset($button['htmlOptions'])) {
				$htmlOptions = $button['htmlOptions'];
				if(!isset($htmlOptions['class'])) {
					$htmlOptions['class'] = 'btn btn-small';
				}
			} else {
				$htmlOptions = array(
					'class' => 'btn btn-small'
				);
			}
			
			if(isset($button['confirm'])) {
				$htmlOptions['onclick'] = 'return confirm("Please, confirm Resolve action")';
			}
			
			$html .= CHtml::link($text, $url, $htmlOptions);
		}
		$html .= '</div>';
		
		return $html;
	}
	
}
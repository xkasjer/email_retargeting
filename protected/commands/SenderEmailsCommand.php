<?php
class SenderEmailsCommand extends Command {

    public function beforeAction($action, $params) {
        $this->addLog('Start Execution script: ' . $action . ' ' . date('d-m-Y H:i:s'));
        return parent::beforeAction($action, $params);
    }

    public function afterAction($action, $params, $exitCode = 0) {
        $this->addLog('End Execution script: ' . $action . ' ' . date('d-m-Y H:i:s'));
        return parent::afterAction($action, $params, $exitCode);
    }

    public function actionPrepareEmail() {
        $oOrders = Order::model()->findAllByAttributes(array('order_status' => Order::ORDER_STATUS_NEW));
        $this->addLog('Order for execution:' . count($oOrders));
        foreach ($oOrders as $oOrder) {
            $oUser = $oOrder->user;
            if (!$oOrder->campaign->checkCampaignActive() || $oUser->active == User::USER_ACTIVE_UNACTIVE) {
                $oOrder->terminateOrder();
                continue;
            }

            if ($oUser->isAvailable() && $oOrder->isOrderAvailable()) {
                $this->addLog('Order available');
                $oOrder->message = new Message();
                $oOrder->message->order_id = $oOrder->order_id;
                $preparePreLanguageHtml = new PreparePreLanguage($oOrder->campaign->creation->html);
                if (!$preparePreLanguageHtml->addFooter($oUser->base->footer, $oUser)) {
                    $this->addLog('Footer from base ' . $oUser->base->base_name . ' is not correct');
                    continue;
                }
                if ($oOrder->campaign->personalization) {
                    $preparePreLanguageHtml->addName($oUser);
                }
                $preparePreLanguageTitle = new PreparePreLanguage($oOrder->campaign->creation->title_email);
                if ($oOrder->campaign->personalization) {
                    $preparePreLanguageTitle->addName($oUser);
                }
                $oOrder->message->message_html = $preparePreLanguageHtml->getHtml();
                $oOrder->message->message_title = $preparePreLanguageTitle->getHtml();
                if ($oOrder->message->save()) {
                    $oOrder->order_status = Order::ORDER_STATUS_ACCEPT;
                    $oOrder->save();
                } else {
                    var_dump($oOrder->message->getErrors());
                }
            }
        }
    }

    public function actionSenderEmail() {
        $oOrders = Order::model()->findAllByAttributes(array('order_status' => Order::ORDER_STATUS_ACCEPT));
        $this->addLog('Order for execution:' . count($oOrders));
        foreach ($oOrders as $oOrder) {
            $replyTo = $from = $oOrder->user->base->email_sender;
            $to = $oOrder->user->email;
            $message = $oOrder->message->message_html;
            $subject = $oOrder->message->message_title;
            $return = EmailPhp::sendEmail($from, '', $to, $subject, $message, $replyTo);

            if ($return) {
                $oOrder->campaign->increaseSendingOrder();
                $oOrder->order_status = Order::ORDER_STATUS_SEND;
                $oOrder->save();
                HourlyCampaignStat::addStatSend($oOrder->campaign_id);
                $this->addLog("Send campaign: " . $oOrder->campaign->campaign_id . " to: " . $to);
            } else {
                $this->addLog("Error to: " . $to);
            }
        }
    }

}

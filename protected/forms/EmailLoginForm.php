<?php
class EmailLoginForm extends CFormModel {

    public $email;
    public $password;
    public $ip;
    private $_identity;

    public function rules() {
        return array(
            array('email, password', 'required'),
            array('password', 'authenticate'),
        );
    }

    /**
     * Authenticates the password.
     * This is the 'authenticate' validator as declared in rules().
     */
    public function authenticate($attribute, $params) {
        if (!$this->hasErrors()) {
            $this->_identity = new UserIdentity($this->email, $this->password);
            if (!$this->_identity->authenticate()) {
                switch ($this->_identity->errorCode) {
                    case UserIdentity::ERROR_USERNAME_INVALID:
                        $this->addError('email', 'This user doesn\'t exist in our database.');
                        break;
                    case UserIdentity::ERROR_PASSWORD_INVALID:
                        $this->addError('password', 'Your password is no correct.');
                        break;
                    default:
                        $this->addError('password', Yii::t('apps', 'login.incorrectPassword'));
                        break;
                }
            }
        }
    }

    public function attributeLabels() {
        return array(
            'email' => 'Email',
            'password' => 'Password',
        );
    }

    /**
     * Logs in the user using the given email and password in the model.
     * @return boolean whether login is successful
     */
    public function login() {
        if ($this->_identity === null) {
            $this->_identity = new UserIdentity($this->email, $this->password);
            $this->_identity->authenticate();
        }
        if ($this->_identity->errorCode === UserIdentity::ERROR_NONE) {
            Yii::app()->user->login($this->_identity);
            return true;
        } else {
            return false;
        }
    }

    public function checkAccessByIpAddress() {
        if ($this->_identity === null) {
            $this->_identity = new UserIdentity($this->email, $this->password);
            $this->_identity->authenticate();
        }
        if ($this->_identity->errorCode === UserIdentity::ERROR_NONE) {
            $this->_identity->checkAccessByIpAddress($this->ip);
        }
        if ($this->_identity->errorCode === UserIdentity::ERROR_NONE) {
            return true;
        }
        if ($this->_identity->errorCode === UserIdentity::ERROR_IP_INVALID) {
            $this->addError('email', 'Access denied.');
        }
        return false;
    }

}

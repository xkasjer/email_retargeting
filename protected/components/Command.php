<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Description of Command
 *
 * @author codibl
 */
class Command extends CConsoleCommand {

    /**
     * stores current log
     * @var string
     */
    public $commandLog = '';

    /**
     * stores current error log
     * @var string
     */
    public $errorLog = '';

    /**
     * default log category
     * @var string
     */
    public $commandLogCategory = 'ConsoleCommand';

    /**
     * default log level
     * @var string
     */
    public $commandLogLevel = CLogger::LEVEL_INFO;

    /**
     * if logs should be displayed on the screen
     * @var boolean
     */
    public $echoCommandLog = true;

    public function log($message) {
        $this->addLog($message);
        $this->flushLog();
    }

    public function addLog($message) {
        $this->commandLog .= $message."\n";
        if ($this->echoCommandLog) {
            echo $message."\n";
        }
    }

    public function addErrorLog($message) {
        $this->errorLog .= $message;
        $this->addLog($message);
    }

    public function flushLog() {
        Yii::getLogger()->log($this->commandLog . "\n", $this->commandLogLevel, $this->commandLogCategory);
        $this->commandLog = '';
    }

    public function isErrorLog() {
        if ($this->errorLog != '') {
            return true;
        }
        return false;
    }

    protected function afterAction($action, $params, $exitCode = 0) {
        if ($this->isErrorLog()) {
            Email::errorEmail($this->errorLog, $this->getName() . " Command Error (" . date('Y-m-d H:i:s') . ")");
        }

        return parent::afterAction($action, $params);
    }

}

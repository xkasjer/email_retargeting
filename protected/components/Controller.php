<?php
class Controller extends CController {

    public $layout = '//layouts/main';
    public $menu = array();
    public $breadcrumbs = array();

    public function filters() {
        return array(
            'accessControl',
            'postOnly + delete',
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'view'),
                'users' => array('@'),
            ),
            array('allow',
                'actions' => array('create', 'update', 'delete'),
                'roles' => array(Login::ROLE_OPERATOR),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

}

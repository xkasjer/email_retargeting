<?php
interface IEmailSender {

    public static function sendEmail($from, $sender, $to, $subject, $message, $replyTo);
}

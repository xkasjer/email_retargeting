<?php
class Model extends CActiveRecord {

    private static function getMapName($field) {
        return '_' . $field . '_map';
    }

    public static function hasOptions($field) {
        return isset(static::${self::getMapName($field)});
    }

    public static function getOptions($field, $subField = null) {
        $map = static::${self::getMapName($field)};
        foreach ($map as $key => $val) {
            if (is_array($val)) {
                foreach ($val as $attr => $attrVal) {
                    if (is_string($attrVal)) {
                        $map[$key][$attr] = Yii::t('model', $attrVal);
                    }
                }
            } elseif (is_string($val)) {
                $map[$key] = Yii::t('model', $val);
            }
        }
        if ($subField != null) {
            $subMap = array();
            foreach ($map as $key => $value) {
                if (isset($value[$subField])) {
                    $subMap[$key] = $value[$subField];
                } else {
                    $subMap[$key] = null;
                }
            }
            return $subMap;
        }
        return $map;
    }

    public static function getDescription($field = null, $value = null) {
        if ($field === null || $value === null) {
            return null;
        }

        if (isset(static::${self::getMapName($field)}[$value])) {
            $item = static::${self::getMapName($field)}[$value];
            if (is_array($item)) {
                foreach ($item as $key => $val) {
                    if (is_string($val)) {
                        $item[$key] = Yii::t('model', $val);
                    }
                }
            } elseif (is_string($item)) {
                $item = Yii::t('model', $item);
            }
            return $item;
        } else {
            return null;
        }
    }

    public function getText($field, $subField = null) {
        if ($this->{$field} === null) {
            return null;
        }
        $description = static::getDescription($field, $this->{$field});
        if ($subField == null && is_string($description)) {
            return $description;
        } elseif ($subField != null && is_array($description)) {
            if (is_string($subField) && isset($description[$subField])) {
                return $description[$subField];
            } else {
                return null;
            }
        } else {
            return 'Field: ' . $field . '[' . $subField . '] of ' . __CLASS__ . ' is not defined';
        }
    }

}

<?php
class UserIdentity extends CUserIdentity {

    protected $_id;

    public function getId() {
        return $this->_id;
    }

    public function authenticate() {
        $oLogin = Login::model()->findByAttributes(array('email' => $this->username));
        if (!$oLogin instanceof Login) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
            
        } elseif (!$oLogin->comparePassword($this->password)) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        } else {
            $this->errorCode = self::ERROR_NONE;
            $this->_id = $oLogin->login_id;
            $this->setState('role', $oLogin->system_operator_id);
        }
        return !$this->errorCode;
    }

}

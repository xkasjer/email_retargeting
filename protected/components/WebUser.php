<?php
class WebUser extends CWebUser {

    public function checkAccess($operation, $params = array()) {

        if (empty($this->id)) {
            return false;
        }
        $role = $this->getState('role');

        // allow access if the operation request is the current user's role
        return ((int) $operation === (int) $role);
    }

    public function isOperator() {
        return (Yii::app()->user->role == Login::ROLE_OPERATOR) ? true : false;
    }
    public function isClient() {
        return (Yii::app()->user->role == Login::ROLE_CLIENT) ? true : false;
    }

}

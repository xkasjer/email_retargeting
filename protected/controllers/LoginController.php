<?php
class LoginController extends Controller {

    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'view', 'create', 'update'),
                'users' => array('@'),
            ),
            array('allow',
                'actions' => array('delete'),
                'roles' => array(Login::ROLE_OPERATOR),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionCreate() {
        $model = new Login;

        $this->performAjaxValidation($model);

        if (isset($_POST['Login'])) {
            $model->attributes = $_POST['Login'];
            $model->password = hash('sha256', $model->password);
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->login_id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        $this->performAjaxValidation($model);

        if (isset($_POST['Login'])) {
            $model->attributes = $_POST['Login'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->login_id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    public function actionIndex() {
        $oLogin = Login::model();

        if (Yii::app()->user->isClient()) {
            $oLogin->client_id = Login::getCurrentUser()->client_id;
        }
        $this->render('index', array(
            'oLogin' => $oLogin,
        ));
    }

    protected function loadModel($id) {
        if (Yii::app()->user->isClient()) {
            $model = Login::model()->findByAttributes(array('login_id' => $id, 'client_id' => Login::getCurrentUser()->client_id));
        } else {
            $model = Login::model()->findByPk($id);
        }
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}

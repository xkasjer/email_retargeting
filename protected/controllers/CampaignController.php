<?php
class CampaignController extends Controller {

    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'view', 'loadHourlyStatsForCampaign', 'statsCampaign'),
                'users' => array('@'),
            ),
            array('allow',
                'actions' => array('create', 'update', 'delete'),
                'roles' => array(Login::ROLE_OPERATOR),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    public function actionView($id) {
        $this->render('view', array(
            'oCampaign' => $this->loadModel($id),
        ));
    }

    public function actionCreate() {
        $model = new Campaign;
        $this->performAjaxValidation($model);

        if (isset($_POST['Campaign'])) {
            $model->attributes = $_POST['Campaign'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->campaign_id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id);
        $this->performAjaxValidation($model);

        if (isset($_POST['Campaign'])) {
            $model->attributes = $_POST['Campaign'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->campaign_id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    public function actionIndex() {
        $oCampaign = Campaign::model();

        if (Yii::app()->user->isClient()) {
            $oCampaign->client_id = Login::getCurrentUser()->client_id;
        }
        $this->render('index', array(
            'oCampaign' => $oCampaign,
        ));
    }

    public function actionStatsCampaign($id) {
        $oCampaign = Campaign::model()->findByPk($id);

        $this->render('stats-campaign', array('oCampaign' => $oCampaign));
    }

    public function actionLoadHourlyStatsForCampaign($id) {
        $date = array();
        $c = new CDbCriteria(array('order' => 'date ASC'));
        $c->compare('campaign_id', $id);
        $c->select = array('date', 'send');
        $aCampaignStats = HourlyCampaignStat::model()->findAll($c);
        foreach ($aCampaignStats as $aCampaignStat) {
            array_push($date, array(strtotime($aCampaignStat->date) * 1000, (int) $aCampaignStat->send));
        }

        echo json_encode($date);
    }

    protected function loadModel($id) {
        if (Yii::app()->user->isClient()) {
            $model = Campaign::model()->findByAttributes(array('campaign_id' => $id, 'client_id' => Login::getCurrentUser()->client_id));
        } else {
            $model = Campaign::model()->findByPk($id);
        }
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist or you have not access.');
        return $model;
    }

    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'campaign-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}

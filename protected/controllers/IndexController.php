<?php
class IndexController extends Controller {

    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('login', 'error'),
                'users' => array('*'),
            ),
            array('allow',
                'actions' => array('logout'),
                'users' => array('@'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest) {
                $oErrorAjaxHandler = new ErrorAjaxHandler('http');
                $oErrorAjaxHandler->httpError($error);
            } else {
                if (Yii::app()->user->isGuest) {
                    Yii::app()->request->redirect('/index/login');
                }
                $this->render('error', $error);
            }
        }
    }

    /**
     * Displays the login page
     */
    public function actionLogin() {
        $this->layout = '//layouts/login';

        if (Yii::app()->user->id != null) {
            $this->redirect(Yii::app()->createUrl("campaign"));
        } else {
            $oEmailLoginForm = new EmailLoginForm();

            if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
                echo CActiveForm::validate($oEmailLoginForm);
                exit;
            }

            if (isset($_POST['EmailLoginForm'])) {
                $oEmailLoginForm->attributes = $_POST['EmailLoginForm'];

                if ($oEmailLoginForm->validate() && $oEmailLoginForm->login()) {
                    $this->redirect(Yii::app()->user->returnUrl);
                }
            }
            $this->render('login', array('model' => $oEmailLoginForm));
        }
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        Yii::app()->user->logout(false);
        $this->redirect(Yii::app()->createUrl("index/login"));
    }

}

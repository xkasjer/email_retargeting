<?php
class RetargetingEngineController extends Controller {

    public function accessRules() {
        return array(
            array('allow',
                'users' => array('*'),
            ),
        );
    }

    public function actionInit($clientId = '', $hash = '') {
        //add funcionaly clientId
        if (Validator::isValidMd5($hash)) {
            Cookie::setCookieValue(array('h' => $hash));
            $oUser = User::model()->findByAttributes(array('md5' => $hash));
            if ($oUser instanceof User) {
                $oUser->updateActivity();
            }
        }
        header("Content-type: image/jpeg");
    }

    public function actionUnsubscribe($email = '', $hash = '') {
        if (Validator::isValidMd5($hash)) {
            $oUser = User::model()->findByAttributes(array('md5' => $hash));
        } else {
            $oUser = User::model()->findByAttributes(array('email' => $email));
        }

        if ($oUser instanceof User) {
            $oUser->unsubscribe();
        } else {
            Yii::app()->end();
        }
        echo "Unsubscribe address ".$oUser->email.' success.';
    }

}

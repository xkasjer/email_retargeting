<?php
class CreationController extends Controller {

    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionCreate() {
        $model = new Creation;

        $this->performAjaxValidation($model);

        if (isset($_POST['Creation'])) {
            $model->attributes = $_POST['Creation'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->campaign_id));
        }

        $criteria = new CDbCriteria();
        $criteria->join = 'LEFT JOIN Creation as c_creation on t.campaign_id = c_creation.campaign_id';
        $criteria->condition = 'c_creation.campaign_id is NULL';
        $oCampaigns = Campaign::model()->findAll($criteria);
        $this->render('create', array(
            'model' => $model,
            'oCampaigns' => $oCampaigns,
        ));
    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        $this->performAjaxValidation($model);

        if (isset($_POST['Creation'])) {
            $model->attributes = $_POST['Creation'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->campaign_id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Creation');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionAdmin() {
        $model = new Creation('search');
        $model->unsetAttributes();
        if (isset($_GET['Creation']))
            $model->attributes = $_GET['Creation'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    protected function loadModel($id) {
        $model = Creation::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'creation-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}

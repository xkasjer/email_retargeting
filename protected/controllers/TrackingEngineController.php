<?php
class TrackingEngineController extends Controller {

    public function actionInit($clientId = '', $campaignId = '') {
        header("Content-type: text/javascript");
        if (!Cookie::issetCookie()) {
            Yii::app()->end();
        }

        $oCampaign = Campaign::model()->findByPk($campaignId);
        if (!$oCampaign instanceof Campaign) {
            Yii::app()->end();
        }
        $hash = Cookie::getCookieHash();
        $oUser = User::model()->findByAttributes(array('active' => User::USER_ACTIVE_ACTIVE, 'md5' => $hash));
        if (!$oUser instanceof User) {
            Yii::app()->end();
        }

        $oOrder = Order::model()->findByAttributes(array('campaign_id' => $campaignId, 'user_id' => $oUser->user_id));
        if ($oOrder instanceof Order) {
            Yii::app()->end();
        }

        if ($oCampaign->checkCampaignActive()) {
            $oOrder = new Order();
            $oOrder->order_status = Order::ORDER_STATUS_NEW;
            $oOrder->user_id = $oUser->user_id;
            $oOrder->campaign_id = $oCampaign->campaign_id;
            $oOrder->save();
        }
    }

    public function actionTermination($campaignId) {
        header("Content-type: text/javascript");
        if (!Cookie::issetCookie()) {
            Yii::app()->end();
        }
        $hash = Cookie::getCookieHash();
        $oUser = User::model()->findByAttributes(array('md5' => $hash));
        if (!$oUser instanceof User) {
            Yii::app()->end();
        }
        $oOrder = Order::model()->findByAttributes(array('campaign_id' => $campaignId, 'user_id' => $oUser->user_id));
        if ($oOrder instanceof Order) {
            $oOrder->terminateOrder();
        }
    }

}

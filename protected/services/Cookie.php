<?php

class Cookie {

    const COOKIE_NAME_RETARGETING = 'retargeting_engine';

    public static function getCookieValue($name, $parse = true) {
        if (self::issetCookie($name)) {
            return ($parse) ? CJSON::decode(Yii::app()->request->cookies[$name]->value) : Yii::app()->request->cookies[$name]->value;
        } else {
            return array();
        }
    }
    public static function getCookieHash() {
        $aCookie = self::getCookieValue(self::COOKIE_NAME_RETARGETING);
        return $aCookie['h'];
    }

    public static function issetCookie($name = self::COOKIE_NAME_RETARGETING) {
        return !!Yii::app()->request->cookies[$name];
    }

    public static function setCookieValue($aNewCookie) {
        $aCookie = self::getCookieValue(self::COOKIE_NAME_RETARGETING);
        foreach ($aNewCookie as $cookieName => $cookieValue) {
            $aCookie[$cookieName] = $cookieValue;
        }
        Yii::app()->request->cookies[self::COOKIE_NAME_RETARGETING] = new CHttpCookie(self::COOKIE_NAME_RETARGETING, CJSON::encode($aCookie));
    }

}

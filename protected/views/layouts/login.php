<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
		<meta charset="utf-8">
		<title><?php echo CHtml::encode($this->pageTitle); ?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="language" content="en" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        <?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/utopia-dark.css'); ?>
        <?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/utopia-responsive.css'); ?>
        <?php Yii::app()->bootstrap->registerResponsiveCss(); ?>
        <?php Yii::app()->bootstrap->registerYiiCss(); ?>
        <?php Yii::app()->bootstrap->registerCoreScripts(); ?>
        <?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/utopiaDark/jquery-ui-1.10.3.utopiaDark.min.css'); ?>
        <!--[if lt IE 8]>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
        <![endif]-->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

		<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<!--[if gte IE 9]>
		  <style type="text/css">
			.gradient {
			   filter: none;
			}
		  </style>
		<![endif]-->

	</head>

	<body>
		<div class="container-fluid">
			<?php echo $content; ?>
		</div>
	</body>
</html>
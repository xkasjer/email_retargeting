<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />

        <!-- blueprint CSS framework -->
        <?php Yii::app()->bootstrap->registerResponsiveCss(); ?>
        <?php Yii::app()->bootstrap->registerYiiCss(); ?>
        <?php Yii::app()->bootstrap->registerCoreScripts(); ?>
        <?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/utopia-dark.css'); ?>
        <?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/utopia-responsive.css'); ?>
        <?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/utopiaDark/jquery-ui-1.10.3.utopiaDark.min.css'); ?>
        <!--[if lt IE 8]>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
        <![endif]-->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

        <?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>		

        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    </head>

    <body>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span12">
                    <div class="header-top" style="padding-right: 15px; padding-left: 5px;">   
                        <div id="logo" style="height: 60px; float: left;"><img src="/images/logo.png" style="height: 100%;" /></div>
                        <div class="header-login" style="float: right; text-align: right; margin-top: 10px;">  
                            <span><?php echo Login::getCurrentUser()->email; ?></span></br>
                            <a href="/index/logout"><span>logout</span></a>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row-fluid">
                <div class="span2">
                    <?php $this->renderPartial('//layouts/partials/_sidebar'); ?> 
                </div>
                <div class="span10">			
                    <?php echo $content; ?>
                </div>
            </div>

            <?php
//        if(isset($this->breadcrumbs)): $this->widget('zii.widgets.CBreadcrumbs', array(
//        'links' => $this->breadcrumbs,
//        ));
//        endif;
            ?>

            <!--            <footer>
                            Copyright &copy; <?php echo date('Y'); ?> by My Company.<br/>
                            All Rights Reserved.<br/>
            <?php echo Yii::powered(); ?>
                        </footer> footer -->

        </div><!-- page -->

    </body>
</html>

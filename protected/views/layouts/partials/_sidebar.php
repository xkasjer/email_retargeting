<div class="sidebar">
    <div class="navbar sidebar-toggle">
        <div class="container">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
        </div>
    </div>

    <div class="nav-collapse collapse leftmenu">
        <ul>
            <li class="outer-element">
                <a class="main" href="<?php echo Yii::app()->createUrl('/campaign/index'); ?>" title="Campaign">
                    <span>Campaign</span></a>
            </li>
            <li class="outer-element">
                <?php $url = (Yii::app()->user->isClient()) ? Yii::app()->createUrl("client/view", array("id" => Login::getCurrentUser()->client_id)) : Yii::app()->createUrl('/client/index'); ?> 
                <a class="clients" href="<?php echo $url; ?>" title="Client">
                    <span>Client</span></a>
            </li>
            <li class="outer-element">
                <a class="availability" href="<?php echo Yii::app()->createUrl('/creation/index'); ?>" title="Creation">
                    <span>Creation</span></a>
            </li>
            <li class="outer-element">
                <a class="availability" href="<?php echo Yii::app()->createUrl('/login/index'); ?>" title="Users">
                    <span>Users</span></a>
            </li>
        </ul>
    </div>
</div>
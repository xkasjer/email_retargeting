<?php
$columns = array(
    array(
        'name' => 'email',
    ),
    array(
        'name' => 'client.client_name',
    ),
    array(
        'class' => 'bootstrap.widgets.TbButtonColumn',
        'updateButtonOptions' => array('class' => 'update open-ajax-box closed'),
        'template' => '{update}',
    )
);
?>

<div class="row-fluid">
    <div class="span12">
        <section class="utopia-widget">
            <div class="utopia-widget-title">
                <span>Users</span>
            </div>
            <div class="utopia-widget-content">
                <div class="row-fluid">
                    <div class="span12">	
                        <?php
                        $this->widget('bootstrap.widgets.TbButtonGroup', array(
                            'buttons' => array(
                                array(
                                    'label' => 'Create New User',
                                    'url' => array('create'),
                                ),
                            )
                        ));
                        ?>	
                        <?php
                        $this->widget('CustomTbGridView', array(
                            'dataProvider' => $oLogin->search(),
                            'columns' => $columns,
                        ));
                        ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<?php
/* @var $this CampaignController */
/* @var $model Campaign */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'campaign-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'client_id'); ?>
		<?php echo $form->dropDownList($model, 'client_id', CHtml::listData(Client::model()->findAll(), 'client_id','client_name')) ?>
		<?php echo $form->error($model,'client_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'campaign_name'); ?>
		<?php echo $form->textField($model,'campaign_name',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'campaign_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date_start'); ?>
		<?php echo $form->textField($model,'date_start'); ?>
		<?php echo $form->error($model,'date_start'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date_end'); ?>
		<?php echo $form->textField($model,'date_end'); ?>
		<?php echo $form->error($model,'date_end'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'campaign_limit'); ?>
		<?php echo $form->textField($model,'campaign_limit'); ?>
		<?php echo $form->error($model,'campaign_limit'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'delay'); ?>
		<?php echo $form->textField($model,'delay'); ?>
		<?php echo $form->error($model,'delay'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'personalization'); ?>
		<?php echo $form->dropDownList($model,'personalization', Campaign::getOptions('campaignPerso')); ?>
		<?php echo $form->error($model,'personalization'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'accept_domain'); ?>
		<?php echo $form->textField($model,'accept_domain'); ?>
		<?php echo $form->error($model,'accept_domain'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
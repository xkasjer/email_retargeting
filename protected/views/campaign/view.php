<section class="utopia-widget">
    <div class="utopia-widget-title">
        <span>Campaign: <?php echo $oCampaign->campaign_name; ?></span>
    </div>
    <div class="utopia-widget-content">
        <div class="row-fluid">
            <div class="span6">
                <?php
                $this->widget('bootstrap.widgets.TbDetailView', array(
                    'data' => $oCampaign,
                    'type' => 'striped condensed',
                    'attributes' => array(
                        'campaign_id',
                        array(
                            'name' => 'client_name',
                            'type' => 'raw',
                            'value' => $oCampaign->client->client_name,
                        ),
                        'campaign_name',
                        'date_start',
                        'date_end',
                    ),
                ));
                ?>
            </div>
            <div class="span6">
                <?php
                $this->widget('bootstrap.widgets.TbDetailView', array(
                    'data' => $oCampaign,
                    'type' => 'striped condensed',
                    'attributes' => array(
                        'campaign_limit',
                        'sending_order',
                        'requirement',
                        array(
                            'name' => 'delay',
                            'type' => 'raw',
                            'value' => $oCampaign->delay.' second'.(($oCampaign->delay>1)?'s':''),
                            ),
                        array(
                            'name' => 'personalization',
                            'type' => 'raw',
                            'value' => $oCampaign->getDescription("campaignPerso",$oCampaign->personalization),
                            ),
                    ),
                ));
                ?>
            </div>
        </div> 
    </div>
</section>
<section class="utopia-widget">
    <div class="utopia-widget-title">
        <span>Campaign code</span>
    </div>
    <div class="utopia-widget-content">
        <div class="row-fluid">
            <?php $this->renderPartial('_campaign-code', array('oCampaign' => $oCampaign)); ?>
        </div>
    </div>
</section>
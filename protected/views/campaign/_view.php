<?php
/* @var $this CampaignController */
/* @var $data Campaign */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('campaign_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->campaign_id), array('view', 'id'=>$data->campaign_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('client_id')); ?>:</b>
	<?php echo CHtml::encode($data->client->client_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('campaign_name')); ?>:</b>
	<?php echo CHtml::encode($data->campaign_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_start')); ?>:</b>
	<?php echo CHtml::encode($data->date_start); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_end')); ?>:</b>
	<?php echo CHtml::encode($data->date_end); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('campaign_limit')); ?>:</b>
	<?php echo CHtml::encode($data->campaign_limit); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('requirement')); ?>:</b>
	<?php echo CHtml::encode($data->requirement); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('delay')); ?>:</b>
	<?php echo CHtml::encode($data->delay); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('personalization')); ?>:</b>
	<?php echo CHtml::encode($data->personalization); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sending_order')); ?>:</b>
	<?php echo CHtml::encode($data->sending_order); ?>
	<br />

	*/ ?>

</div>
<script src="http://code.highcharts.com/stock/highstock.js"></script>
<script>
    $(function () {
        $.getJSON('/campaign/loadHourlyStatsForCampaign/<?php echo $oCampaign->campaign_id; ?>', function (data) {

            // create the chart
            $('#container').highcharts('StockChart', {
                chart: {
                    alignTicks: false
                },
                rangeSelector: {
                    selected: 1
                },
                title: {
                    text: '<?php echo $oCampaign->campaign_name; ?>'
                },
                series: [{
                        type: 'column',
                        name: '<?php echo $oCampaign->campaign_name; ?>',
                        data: data,
                        dataGrouping: {
                            units: [[
                                    'week', // unit name
                                    [1] // allowed multiples
                                ], [
                                    'month',
                                    [1, 2, 3, 4, 6]
                                ]]
                        }
                    }]
            });
        });
    });
</script>

<div id="container" style="height: 400px"></div>
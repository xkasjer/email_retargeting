<?php
$template = (Yii::app()->user->isOperator()) ? '{update}' : '';
$columns = array(
    array(
        'name' => 'client_name',
        'type' => 'raw',
        'value' => '$data->client->client_name',
    ),
    'campaign_name',
    'date_start',
    'date_end',
    'campaign_limit',
    'sending_order',
    array(
        'type' => 'raw',
        'value' => 'ButtonGroup::display(array(
			array(
				"text" => "Charts",
				"url" => Yii::app()->createUrl("campaign/statsCampaign",array("id"=>$data->primaryKey)),
			),array(
				"text" => "Details",
				"url" => Yii::app()->createUrl("campaign/view",array("id"=>$data->primaryKey)),
			),
		))',
        'header' => 'Options',
        'sortable' => false,
        'filter' => false,
        'htmlOptions' => array('style' => 'width:120px;'),
    ),
    array(
        'class' => 'bootstrap.widgets.TbButtonColumn',
        'updateButtonOptions' => array('class' => 'update open-ajax-box closed'),
        'template' => $template,
    )
);
?>

<div class="row-fluid">
    <div class="span12">
        <section class="utopia-widget">
            <div class="utopia-widget-title">
                <span>Campaigns</span>
            </div>
            <div class="utopia-widget-content">
                <div class="row-fluid">
                    <div class="span12">	
                        <?php
                        if (Yii::app()->user->isOperator()) {
                            $this->widget('bootstrap.widgets.TbButtonGroup', array(
                                'buttons' => array(
                                    array('label' => 'Create Campaign', 'url' => array('create')),
                                )
                            ));
                        }
                        ?>	
                        <?php
                        $this->widget('CustomTbGridView', array(
                            'dataProvider' => $oCampaign->search(),
                            'columns' => $columns,
                        ));
                        ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
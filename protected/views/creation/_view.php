<?php
/* @var $this CreationController */
/* @var $data Creation */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('campaign_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->campaign_id), array('view', 'id'=>$data->campaign_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

</div>
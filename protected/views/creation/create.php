<?php
/* @var $this CreationController */
/* @var $model Creation */

$this->breadcrumbs=array(
	'Creations'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Creation', 'url'=>array('index')),
	array('label'=>'Manage Creation', 'url'=>array('admin')),
);
?>

<h1>Create Creation</h1>

<?php $this->renderPartial('_form', array('model'=>$model,'oCampaigns'=>$oCampaigns)); ?>
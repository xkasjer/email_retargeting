<?php
/* @var $this CreationController */
/* @var $model Creation */

$this->breadcrumbs=array(
	'Creations'=>array('index'),
	$model->campaign_id=>array('view','id'=>$model->campaign_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Creation', 'url'=>array('index')),
	array('label'=>'Create Creation', 'url'=>array('create')),
	array('label'=>'View Creation', 'url'=>array('view', 'id'=>$model->campaign_id)),
	array('label'=>'Manage Creation', 'url'=>array('admin')),
);
?>

<h1>Update Creation <?php echo $model->campaign_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
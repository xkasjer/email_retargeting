<?php
/* @var $this CreationController */
/* @var $model Creation */

$this->breadcrumbs = array(
    'Creations' => array('index'),
    $model->campaign_id,
);

$this->menu = array(
    array('label' => 'List Creation', 'url' => array('index')),
    array('label' => 'Create Creation', 'url' => array('create')),
    array('label' => 'Update Creation', 'url' => array('update', 'id' => $model->campaign_id)),
    array('label' => 'Delete Creation', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->campaign_id), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => 'Manage Creation', 'url' => array('admin')),
);
?>

<h1>View Creation #<?php echo $model->campaign_id; ?></h1>

<?php
$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'campaign_id',
        'description',
    ),
));
?>

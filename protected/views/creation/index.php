<?php
$columns = array(
    array(
        'name' => 'campaign_name',
        'type' => 'raw',
        'value' => '$data->campaign->campaign_name',
    ),
    array(
        'class' => 'bootstrap.widgets.TbButtonColumn',
        'updateButtonOptions' => array('class' => 'update open-ajax-box closed'),
        'template' => '{update}',
    )
);
?>

<div class="row-fluid">
    <div class="span12">
        <section class="utopia-widget">
            <div class="utopia-widget-title">
                <span>Creations</span>
            </div>
            <div class="utopia-widget-content">
                <div class="row-fluid">
                    <div class="span12">	
                        <?php
                        $this->widget('bootstrap.widgets.TbButtonGroup', array(
                            'buttons' => array(
                                array('label' => 'Create Creation', 'url' => array('create')),
                                array('label' => 'Manage Creation', 'url' => array('admin')),
                            )
                        ));
                        ?>	
                        <?php
                        $this->widget('CustomTbGridView', array(
                            'dataProvider' => $dataProvider,
                            'columns' => $columns,
                        ));
                        ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>


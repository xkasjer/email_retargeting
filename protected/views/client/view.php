<section class="utopia-widget">
    <div class="utopia-widget-title">
        <span>Client: <?php echo $oClient->client_name; ?></span>
    </div>
    <div class="utopia-widget-content">
        <div class="row-fluid">
            <div class="span6">
                <?php
                $this->widget('bootstrap.widgets.TbDetailView', array(
                    'data' => $oClient,
                    'type' => 'striped condensed',
                    'attributes' => array(
                        'client_name',
                        'client_address',
                        'client_country',
                    ),
                ));
                ?>
            </div>
            <div class="span6">
                <?php
                $this->widget('bootstrap.widgets.TbDetailView', array(
                    'data' => $oClient,
                    'type' => 'striped condensed',
                    'attributes' => array(
                        'client_contact_person',
                        'client_phone',
                        'client_email',
                        'client_timezone',
                    ),
                ));
                ?>
            </div>
        </div> 
    </div>
</section>
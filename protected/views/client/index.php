<?php
$template = (Yii::app()->user->isOperator()) ? '{update}' : '';
$columns = array(
    'client_name',
    'client_contact_person',
    'client_phone',
    'client_email',
    'client_country',
    array(
        'type' => 'raw',
        'value' => 'ButtonGroup::display(array(
			array(
				"text" => "Details",
				"url" => Yii::app()->createUrl("client/view",array("id"=>$data->primaryKey)),
			),
		))',
        'header' => 'Options',
        'sortable' => false,
        'filter' => false,
        'htmlOptions' => array('style' => 'width:60px;'),
    ),
    array(
        'class' => 'bootstrap.widgets.TbButtonColumn',
        'updateButtonOptions' => array('class' => 'update open-ajax-box closed'),
        'template' => $template,
    )
);
?>

<div class="row-fluid">
    <div class="span12">
        <section class="utopia-widget">
            <div class="utopia-widget-title">
                <span>Clients</span>
            </div>
            <div class="utopia-widget-content">
                <div class="row-fluid">
                    <div class="span12">	
                        <?php
                        if (Yii::app()->user->isOperator()) {
                            $this->widget('bootstrap.widgets.TbButtonGroup', array(
                                'buttons' => array(
                                    array(
                                        'label' => 'Create Client',
                                        'url' => array('create'),
                                    ),
                                )
                            ));
                        }
                        ?>	
                        <?php
                        $this->widget('CustomTbGridView', array(
                            'dataProvider' => $oClient->search(),
                            'columns' => $columns,
                        ));
                        ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="utopia-login-message">
            <div class="os-phrases" id="os-phrases">
                <h2>Welcome to Email Retargeting System</h2>
            </div>
            <p>Sign in to get access</p>
        </div>
    </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <div class="row-fluid">
            <div class="span3" style="margin: 0 auto; float: none;">
                <div class="utopia-login" style="margin: 0 auto;">
                    <?php
                    $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'login-form',
                        'htmlOptions' => array('class' => 'utopia'),
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                            'validateOnChange' => false,
                            'validateOnType' => false
                        )
                    ));
                    ?>
                    <?php
                    echo $form->labelEx($model, 'email');
                    echo $form->textField($model, 'email', array('class' => 'span12 utopia-fluid-input validate[required]'));
                    echo $form->error($model, 'email');
                    ?>

                    <?php
                    echo $form->labelEx($model, 'password', array('class' => 'control-label'));
                    echo $form->passwordField($model, 'password', array('class' => 'span12 utopia-fluid-input validate[required]'));
                    echo $form->error($model, 'password');
                    ?>

                    <div class="utopia-login-action">
                        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'icon' => 'ok', 'label' => 'Submit')); ?>
                    </div>
                    <?php $this->endWidget(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
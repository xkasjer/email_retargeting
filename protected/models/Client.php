<?php
class Client extends CActiveRecord {

    public function tableName() {
        return 'Client';
    }

    public function rules() {
        return array(
            array('client_name', 'required'),
            array('client_email', 'email'),
            array('client_name, client_address', 'length', 'max' => 150),
            array('client_id, client_contact_person, client_phone, client_email, client_timezone, client_country', 'length', 'max' => 100),
            array('client_id, client_name, client_contact_person, client_phone, client_email, client_timezone', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
            'campaigns' => array(self::HAS_MANY, 'Campaign', 'client_id'),
        );
    }

    public function attributeLabels() {
        return array(
            'client_id' => 'Client',
            'client_name' => 'Client Name',
            'client_contact_person' => 'Client Person',
            'client_phone' => 'Person Contact Phone',
            'client_email' => 'Person Contact Email',
            'client_timezone' => 'Timezone',
            'client_country' => 'Country',
            'client_address' => 'Address',
        );
    }

    /** @return CActiveDataProvider */
    public function search() {
        $criteria = new CDbCriteria;
        $criteria->compare('client_id', $this->client_id);
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}

<?php
class User extends Model {

    const USER_ACTIVE_ACTIVE = 1;
    const USER_ACTIVE_UNACTIVE = 0;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function relations() {
        return array(
            'base' => array(self::BELONGS_TO, 'Base', 'base_id'),
            'name' => array(self::BELONGS_TO, 'Name', 'name_id'),
        );
    }

    public function rules() {
        return array(
            array('last_activity', 'safe')
        );
    }

    public function updateActivity() {
        $this->last_activity = date('Y-m-d H:i:s');
        $this->save();
    }

    public function unsubscribe() {
        $this->active = self::USER_ACTIVE_UNACTIVE;
        $this->save();
    }

    public function isAvailable() {
        if (($this->last_send + $this->base->ttl) < time()) {
            return true;
        }
        return true;
    }

}

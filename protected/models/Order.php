<?php
class Order extends Model {

    const ORDER_STATUS_ERROR = -1;
    const ORDER_STATUS_CANCEL = 0;
    const ORDER_STATUS_NEW = 1;
    const ORDER_STATUS_ACCEPT = 2;
    const ORDER_STATUS_SEND = 3;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function relations() {
        return array(
            'campaign' => array(self::BELONGS_TO, 'Campaign', 'campaign_id'),
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
            'message' => array(self::HAS_ONE, 'Message', 'order_id'),
        );
    }

    public function rules() {
        return array(
            array('order_status, campaign_id, user_id', 'safe')
        );
    }

    public function terminateOrder() {
        //$this->order_status = self::ORDER_STATUS_CANCEL;
        $this->save();
    }

    public function isOrderAvailable() {
        if (($this->create_at + $this->campaign->delay) < time()) {
            return true;
        }
        return false;
    }

}

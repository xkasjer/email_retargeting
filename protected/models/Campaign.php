<?php
class Campaign extends Model {

    const CAMPAIGN_PERSO_NO = 0;
    const CAMPAIGN_PERSO_YES = 1;

    protected static $_campaignPerso_map = array(
        self::CAMPAIGN_PERSO_NO => 'No',
        self::CAMPAIGN_PERSO_YES => 'Yes',
    );

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function relations() {
        return array(
            'client' => array(self::BELONGS_TO, 'Client', 'client_id'),
            'creation' => array(self::HAS_ONE, 'Creation', 'campaign_id'),
        );
    }

    public function rules() {
        return array(
            array('campaign_name, date_start, date_end, campaign_limit, personalization, accept_domain, client_id, delay', 'safe')
        );
    }

    public function checkCampaignActive() {
        $dateNow = date('Y-m-d H:i:s');
        if ($this->date_start > $dateNow || $this->date_end < $dateNow) {
            return false;
        }
        if ($this->campaign_limit < $this->sending_order) {
            return false;
        }
        return true;
    }

    public function increaseSendingOrder() {
        $this->sending_order++;
        $this->update();
    }

    /** @return CActiveDataProvider */
    public function search() {
        $criteria = new CDbCriteria;
        $criteria->compare('client_id', $this->client_id);
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}

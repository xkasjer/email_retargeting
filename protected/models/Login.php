<?php
class Login extends CActiveRecord {

    const ROLE_OPERATOR = 1;
    const ROLE_CLIENT = 2;

    public function rules() {
        return array(
            array('email, password, system_operator_id', 'required'),
            array('client_id, system_operator_id', 'numerical', 'integerOnly' => true),
            array('email', 'length', 'max' => 60),
            array('password', 'length', 'max' => 64),
            array('login_id, email, password, client_id, system_operator_id', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
            'client' => array(self::BELONGS_TO, 'Client', 'client_id'),
        );
    }

    public function attributeLabels() {
        return array(
            'login_id' => 'Login',
            'email' => 'Email',
            'password' => 'Password',
            'client_id' => 'Client',
            'system_operator_id' => 'System Operator',
        );
    }

    /** @return CActiveDataProvider */
    public function search() {
        $criteria = new CDbCriteria;
        $criteria->compare('client_id', $this->client_id);
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function comparePassword($password) {
        if (hash('sha256', $password) === $this->password) {
            return true;
        }
        return false;
    }

    public static function getCurrentUser() {
        $oLogin = self::model()->findByPk(Yii::app()->user->id);
        return ($oLogin instanceof Login) ? $oLogin : false;
    }

}

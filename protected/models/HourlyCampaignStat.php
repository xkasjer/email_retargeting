<?php
class HourlyCampaignStat extends Model {

    public function tableName() {
        return 'HourlyCampaignStat';
    }

    public function rules() {
        return array(
            array('campaign_id, date', 'required'),
            array('campaign_id, send', 'numerical', 'integerOnly' => true),
            array('daily_stat_id, campaign_id, date, send', 'safe'),
        );
    }

    public function relations() {
        return array();
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function increseDailyStat() {
        $this->send++;
        $this->save();
    }

    public static function AddStatSend($campaignId) {
        $oStat = self::model()->findByAttributes(array('campaign_id' => $campaignId, 'date' => date('Y-m-d H:00:00')));
        if ($oStat instanceof HourlyCampaignStat) {
            $oStat->increseDailyStat();
        } else {
            $oStat = new HourlyCampaignStat();
            $oStat->date = date('Y-m-d H:00:00');
            $oStat->campaign_id = $campaignId;
            $oStat->increseDailyStat();
        }
    }

}
